# Description

This rtlwifi source is from Linux kernel upstream source tree

Attention! this is a DKMS module, so it need to install using DKMS.

# Changelogs
16/July/2018
- Updated from upstream.
- Added rtl818x supports.
- Don't use mode debugging patch.
- Changed to date versioning.
- Fixed Compilation error, Makeconfig.

20/June/2018
- Updated from upstream.

# Installation
1. Clone or download this repo, if you download it, you must extract first.
2. Add it to DKMS as below

   <code>sudo dkms add ./Realtek-rtlwifi-DKMS-upstream-master/</code>
3. Install it using DKMS as below

   <code>sudo dkms install rtlwifi-upstream/(master / version) --force</code>
4. Reboot or restart your GNU/Linux machine to check are your Realtek RTL series is functioned.

# Uninstall
1. Uninstall it using DKMS as below

   <code>sudo dkms remove rtlwifi-upstream/(master / version) --all</code>
4. Reboot or restart your GNU/Linux machine.
